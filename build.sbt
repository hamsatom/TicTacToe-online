name := "TicTacToeBot"

version := "2.0"

lazy val `tictactoebot` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "https://repo.akka.io/snapshots/"

scalaVersion := "2.13.8"

libraryDependencies ++= Seq(jdbc, ehcache, ws, specs2 % Test, guice)

Test / unmanagedResourceDirectories += baseDirectory(_ / "target/web/public/test").value

      