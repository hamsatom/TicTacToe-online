package controllers

import logic.player.{AlphaBetaPlayer, Player}
import model.Board
import play.api.Logger
import play.api.libs.json._
import play.api.mvc.{AbstractController, Action, ControllerComponents, Request}

import javax.inject.{Inject, Singleton}

@Singleton
class MoveController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  private final val logger: Logger = Logger(this.getClass)
  private final val player: Player = new AlphaBetaPlayer

  private implicit val moveWrites: Writes[Long] = (move: Long) => {
    val bitIndex = java.lang.Long.numberOfLeadingZeros(move)
    Json.obj(
      "x" -> bitIndex / 8,
      "y" -> bitIndex % 8
    )
  }

  def makeMove(playerOnMove: Short): Action[JsValue] =
    Action(parse.json) {
      val startTime = System.currentTimeMillis()
      implicit request: Request[JsValue] =>
        try {
          val playerMarks = request.body.as[Array[String]]
          val sideToMove = playerOnMove - 1
          val board = Board.construct(playerMarks, sideToMove)
          player.makeMove(board, startTime, sideToMove)
            .map(move => Json.toJson(move))
            .map(json => Ok(json).as(JSON))
            .getOrElse(BadRequest("Can't place any move on board " + board))
        } catch {
          case ex: Exception =>
            logger.error("Exception occurred", ex)
            InternalServerError("Exception occurred")
          case er: Error =>
            logger.error("Error occurred", er)
            InternalServerError("Error occurred")
        }
    }
}