package logic.player

import logic.player.AlphaBetaPlayer.{MAX_SCORE, MIN_SCORE, TIME_LIMIT}
import logic.{BoundType, Bounds, Heuristic}
import model.{Board, Move}

import java.util.concurrent.CountDownLatch
import scala.annotation.switch
import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration.MILLISECONDS
import scala.util.control.Breaks.{break, breakable}

class AlphaBetaPlayer extends Player {
  private final var transpositionTable = new mutable.HashMap[Long, Bounds]

  override protected def makeMoveInternal(board: Board, startTime: Long, possibleMoves: Long, sideOnMove: Int, sideOffMove: Int): Long = {
    var bestMove = java.lang.Long.lowestOneBit(possibleMoves)
    try {
      val marksCount = board.countMarks()
      transpositionTable = transpositionTable.filter(entry => entry._2.marksCount >= marksCount)

      val maybeBounds = transpositionTable.get(board.zobristHash)
      if (maybeBounds.isDefined) {
        val bounds = maybeBounds.get
        if (bounds.score >= MAX_SCORE) {
          return bounds.bestMove
        }
      }

      var movesLeft = possibleMoves
      var bestScore = MIN_SCORE
      var scoredMoves = mutable.Buffer[Move]()
      while (movesLeft != 0L) {
        val move = java.lang.Long.lowestOneBit(movesLeft)
        board.makeMove(move, sideOnMove)
        if (board.hasFiveConnected(sideOnMove)) {
          store(board, 1, MIN_SCORE, MAX_SCORE, maybeBounds, move, MAX_SCORE)
          return move
        }
        val score = Heuristic.evaluate(board, sideOnMove, sideOffMove)
        board.unmakeMove(move, sideOnMove)
        if (score > bestScore) {
          bestScore = score
          bestMove = move
        }
        scoredMoves.addOne(new Move(move, score))

        movesLeft = (movesLeft - 1) & movesLeft
      }
      val bounds = Some(store(board, 1, MIN_SCORE, bestScore, maybeBounds, bestMove, bestScore))

      val latch = new CountDownLatch(1)
      Future {
        breakable {
          for (depth <- 1 to 8) {
            scoredMoves = scoredMoves.sorted
            bestScore = MIN_SCORE
            var depthBestMove = bestMove

            for (move <- scoredMoves) {
              val mask = move.mask
              board.makeMove(mask, sideOnMove)
              val score = -negaScout(board, sideOffMove, sideOnMove, depth, MIN_SCORE, -bestScore)
              if (score > bestScore) {
                if (score >= MAX_SCORE) {
                  store(board, depth + 1, MIN_SCORE, score, bounds, mask, score)
                  bestMove = mask
                  latch.countDown()
                  break()
                }
                depthBestMove = mask
                bestScore = score
              }
              if (TIME_LIMIT >= startTime - System.currentTimeMillis()) {
                latch.countDown()
                break()
              }
              board.unmakeMove(mask, sideOnMove)
              move.score = score
            }

            store(board, depth + 1, MIN_SCORE, bestScore, bounds, depthBestMove, bestScore)
            bestMove = depthBestMove
          }
          latch.countDown()
        }
      }
      latch.await(startTime - TIME_LIMIT - System.currentTimeMillis(), MILLISECONDS)

    } catch {
      case ex: Exception => ex.printStackTrace()
      case er: Error => er.printStackTrace()
    }
    return bestMove
  }

  private def negaScout(board: Board, sideOnMove: Int, sideOffMove: Int, depth: Int, alpha: Int, beta: Int): Int = {
    var newAlpha = alpha
    var newBeta = beta
    val bounds = transpositionTable.get(board.zobristHash)
    if (bounds.isDefined) {
      val bound = bounds.get
      if (bound.depth >= depth) {
        bound.boundType match {
          case BoundType.EXACT => return bound.score
          case BoundType.LOWER =>
            if (bound.score > newAlpha) {
              if (bound.score >= newBeta) {
                return bound.score
              }
              newAlpha = bound.score
            }
          case BoundType.UPPER =>
            if (bound.score < newBeta) {
              if (bound.score <= newAlpha) {
                return bound.score
              }
              newBeta = bound.score
            }
        }
      }
    }

    if (board.hasFiveConnected(sideOffMove)) {
      return MIN_SCORE
    } else if (depth <= 0) {
      return Heuristic.evaluate(board, sideOnMove, sideOffMove)
    }
    val possibleMoves = board.findEmptyFields
    if (possibleMoves == 0L) {
      return 0
    }

    var bestMove = bounds.map(_.bestMove).getOrElse(java.lang.Long.lowestOneBit(possibleMoves))
    val nextDepth = depth - 1
    board.makeMove(bestMove, sideOnMove)
    var value = -negaScout(board, sideOffMove, sideOnMove, nextDepth, -newBeta, -newAlpha)
    board.unmakeMove(bestMove, sideOnMove)

    if (value < newBeta) {
      var a = value.max(newAlpha)
      var b = a + 1
      var movesLeft = possibleMoves ^ bestMove

      breakable {
        while (movesLeft != 0L) {
          val move = java.lang.Long.lowestOneBit(movesLeft)
          board.makeMove(move, sideOnMove)
          var v = -negaScout(board, sideOffMove, sideOnMove, nextDepth, -b, -a)
          if (v > a && v < newBeta) {
            v = -negaScout(board, sideOffMove, sideOnMove, nextDepth, -newBeta, -v)
          }
          board.unmakeMove(move, sideOnMove)

          if (v > value) {
            value = v
            bestMove = move
            if (value > a) {
              if (value >= newBeta) {
                break()
              }
              a = value
              b = a + 1
            }
          }
          movesLeft = (movesLeft - 1) & movesLeft
        }
      }
    }

    store(board, depth, newAlpha, newBeta, bounds, bestMove, value)
    return value
  }

  private def store(board: Board, depth: Int, alpha: Int, beta: Int, bounds: Option[Bounds], bestMove: Long, value: Int): Bounds = {
    if (bounds.isEmpty) {
      val boundType: BoundType.Value = (value: @switch) match {
        case _ if value <= alpha => BoundType.UPPER
        case _ if value >= beta => BoundType.LOWER
        case _ => BoundType.EXACT
      }
      val bound = new Bounds(boundType, value, depth, bestMove, board.countMarks())
      transpositionTable.put(board.zobristHash, bound)
      return bound
    } else {
      val bound = bounds.get
      if (bound.depth < depth || bound.depth == depth && bound.score <= value) {
        bound.boundType = (value: @switch) match {
          case _ if value <= alpha => BoundType.UPPER
          case _ if value >= beta => BoundType.LOWER
          case _ => BoundType.EXACT
        }
        bound.score = value
        bound.depth = depth
        bound.bestMove = bestMove
      }
      return bound
    }
  }
}

private object AlphaBetaPlayer {
  private final val MAX_SCORE = 100_000
  private final val MIN_SCORE = -MAX_SCORE
  private final val TIME_LIMIT = -1744
}