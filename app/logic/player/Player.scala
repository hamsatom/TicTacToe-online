package logic.player

import model.Board

trait Player {
  def makeMove(board: Board, startTime: Long, sideOnMove: Int): Option[Long] = {
    val possibleMoves = board.findEmptyFields
    if (java.lang.Long.bitCount(possibleMoves) == 1) {
      return Some(possibleMoves)
    }
    if (possibleMoves == 0L || board.hasFiveConnected(sideOnMove)) {
      return None
    }
    val sideOffMove = 1 - sideOnMove
    if (board.hasFiveConnected(sideOffMove)) {
      return None
    }

    return Some(makeMoveInternal(board, startTime, possibleMoves, sideOnMove, sideOffMove))
  }

  protected def makeMoveInternal(board: Board, startTime: Long, possibleMoves: Long, sideOnMove: Int, sideOffMove: Int): Long = return java.lang.Long.lowestOneBit(possibleMoves)
}
