package logic

import model.Board
import model.Board.WIN_MASKS

protected object Heuristic {

  def evaluate(board: Board, sideOnMove: Int, sideOffMove: Int): Int = {
    val onMoveMarks = board.getPlayerMarks(sideOnMove)
    val offMoveMarks = board.getPlayerMarks(sideOffMove)
    var score = 0

    for (mask <- WIN_MASKS) {
      if ((mask & onMoveMarks) == 0L) {
        val markCount = java.lang.Long.bitCount(mask & offMoveMarks)
        if (markCount > 0) {
          score -= evaluateMask(markCount)
        }
      } else if ((mask & offMoveMarks) == 0L) {
        val markCount = java.lang.Long.bitCount(mask & onMoveMarks)
        if (markCount > 0) {
          score += evaluateMask(markCount)
        }
      }
    }

    return score
  }

  private def evaluateMask(markCount: Int): Int = {
    return markCount * 10 + (5 - markCount)
  }
}
