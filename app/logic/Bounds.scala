package logic

protected class Bounds(var boundType: BoundType.Value, var score: Int, var depth: Int, var bestMove: Long, val marksCount: Int) {
}

protected object BoundType extends Enumeration {
  type BoardType = Value
  val LOWER, UPPER, EXACT = Value
}
