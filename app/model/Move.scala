package model

class Move(val mask: Long, var score: Int) extends Ordered[Move] {
  override def compare(that: Move): Int = return that.score - score
}