[![pipeline status](https://gitlab.fel.cvut.cz/hamsatom/TicTacToe-online/badges/backend/pipeline.svg)](https://gitlab.fel.cvut.cz/hamsatom/TicTacToe-online/commits/backend)

**Autor:** Tomáš Hamsa  
**Semestr:** letní 2017/2018 - B172  
**Předmět:**  MI-PSL - programování v jazyku Scala
# TicTacToe bot
AlphaBeta bot na piškvorky s REST API v Play Scala  
Backend nasazen na Heroku hostingu, frontend na GitLab pages na adrese http://hamsatom.pages.fel.cvut.cz/TicTacToe-online/
